
---

# 實作步驟

1. 先登入GitLab，並且創建New project，Project name：GibLabGitBookCreate  
   Project slug 預設會跟Project name一樣。  
   ![](/assets/A01.jpg)

2. 開啟GitBook Editor，但是不要登入，按下Do that later  
   ![](/assets/A02.jpg)

3. 新建一本書，按下New Book  
   ![](/assets/A03.jpg)

4. 新建的書名跟Project name：GibLabGitBookCreate一樣。  
   ![](/assets/A04.jpg)

5. 特別注意，建立新章節，不要空白，GitBook Editor會錯亂。  
   ![](/assets/A05.jpg)

6. 按下存檔，將剛剛打的書儲存。  
   ![](/assets/A06.jpg)

7. 按下Publish，更新到GitLab  
   ![](/assets/A07.jpg)

8. 這邊會要輸入網址  
   ![](/assets/A08.jpg)

9. 直接輸入我們剛剛在GitLab建立新專案的網址  
   [https://gitlab.com/cit3753404/gitlabgitbookcreate!\[\]\(/assets/A09.jpg](https://gitlab.com/cit3753404/gitlabgitbookcreate![]\(/assets/A09.jpg\)\)

10. 接著會要求我們登入Gitlab![](/assets/A10.jpg)

11. 輸入GitLab的帳號密碼![](/assets/A11.jpg)

12. 回到GitLab的網站，重新整理之後可以看到剛剛輸入的畫面，已經更新。![](/assets/A12.jpg)

13. 接下來我們要上傳一個腳本.gitlab-ci.yml到剛剛專案的GitLab上面。  
    ![](/assets/A13.jpg)

14. 從剛剛的專案，按下+號，選擇Upload file  
    ![](/assets/A14.jpg)

15. 選入檔案之後，按下Upload file  
    ![](/assets/A15.jpg)

16. 按一下Edit  
    ![](/assets/A16.jpg)

17. 按一下Commit changes  
    ![](/assets/A17.jpg)

18. 回到專案，會看到GitLab正在Pipeline:running  
    ![](/assets/A18.jpg)

19. 跑完之後會呈現，Pipeline:passed  
    ![](/assets/A19.jpg)

20. 選擇Settings&gt;Pages，就可以看到這本書發布的網頁連結了  
    ![](/assets/A20.jpg)

21. 點開連結，就會跳出剛剛我們新增加的那本書的網頁。  
    ![](/assets/A21.jpg)

22. 日後，只要將GitBook Editor編輯好的書，按下發布即可。  
    經測試，只要在GitBook Editor按下發布，GitLab就會自動編譯。  
    ~~\(然後去GitLab專案裡面按下Edit，Commit changes，GitLab，就會自動把最新的內容編譯發布。\)~~

23. kk

24. kk



